# MarketSimplified

# Technologies Used
1. Kotlin
2. Jetpack components
   - Navigation components
   - Hilt
   - DataBinding
   - Paging Library 3
3. Room
4. Retrofit
5. Glide
6. MVVM Pattern


