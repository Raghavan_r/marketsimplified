package com.app.demo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.app.demo.databinding.CommentListBinding
import com.app.demo.network.Item

class DetailsAdapter(
        private val viewLifecycleOwner: LifecycleOwner
) :
        RecyclerView.Adapter<DetailsAdapter.DetailsViewHolder>() {

    private var list = emptyList<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = CommentListBinding.inflate(layoutInflater, parent, false)
        itemBinding.lifecycleOwner = viewLifecycleOwner
        return DetailsViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: DetailsViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount() = list.size

    fun setDataList(list: List<String>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class DetailsViewHolder(private val itemBinding: CommentListBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        val model = DataBinder()

        fun bind(article: String) {
            itemBinding.binder = model
            model.detailsBind(article)
        }

    }

}