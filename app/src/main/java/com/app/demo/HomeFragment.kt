package com.app.demo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.paging.LoadState
import androidx.paging.PagingData
import com.app.demo.databinding.FragmentHomeBinding
import com.app.demo.network.Item
import com.app.demo.network.Response
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : BaseFragment() {

    private lateinit var binding: FragmentHomeBinding
    private val homeViewModel: HomeViewModel by activityViewModels()

    private val adapter by lazy {
        HomeAdapter(object : ClickListener {
            override fun onItemClick(value: String, position: Int) {
                homeViewModel.updateComment(value,position)
            }
            override fun onPositionClick(item: Item) {
                Navigation.findNavController(binding.root)
                        .navigate(R.id.action_homeFragment_to_detailsFragment,
                                Bundle().apply {
                                    this.putInt("ID_DATA", item.id)
                                })
            }
        }, this)
    }


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
                layoutInflater,
                R.layout.fragment_home,
                container,
                false
        )
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvData.adapter = adapter
        homeViewModel.apiError.observe(viewLifecycleOwner, {
            showSnackBar(it)
        })
        homeViewModel.loading.observe(viewLifecycleOwner,{
            showProgress(it)
        })
        homeViewModel.data.observe(viewLifecycleOwner, {
            if (it != null) {
                setListAdapter(it)
            }
        })


        adapter.addLoadStateListener { loadState ->
            (loadState.source.refresh as? LoadState.Error)?.error?.let {
                it.message?.let { it1 -> showSnackBar(it1) }
            }
        }
    }

    private fun setListAdapter(pagingData: PagingData<Item>) {

        homeViewModel.stopListJob()
        homeViewModel.viewListJob = lifecycleScope.launch {
            adapter.submitData(pagingData)
        }

    }


}