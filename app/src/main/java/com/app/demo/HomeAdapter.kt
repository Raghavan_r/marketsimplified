package com.app.demo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.demo.databinding.ItemListBinding
import com.app.demo.network.Item

class HomeAdapter(
        private val adapterItemClickListener: ClickListener,
        private val lifecycleOwner: LifecycleOwner
) : PagingDataAdapter<Item, RecyclerView.ViewHolder>(SEARCH_COMPARATOR)

{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardSearchViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemListBinding.inflate(layoutInflater, parent, false)
        itemBinding.lifecycleOwner = lifecycleOwner
        return DashboardSearchViewHolder(
                itemBinding
        )
    }


    companion object {
        private val SEARCH_COMPARATOR = object : DiffUtil.ItemCallback<Item>() {
            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean =
                    oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean =
                    oldItem == newItem
        }
    }

    inner class DashboardSearchViewHolder(
            private val itemBinding: ItemListBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        private val binder = DataBinder()

        init {
            itemBinding.binder = binder
        }

        @MainThread
        fun bind(searchResponse: Item, position: Int) {
            itemBinding.ivSend.setOnClickListener {
                adapterItemClickListener.onItemClick(itemBinding.etComment.text.toString(),searchResponse.id)
                itemBinding.etComment.setText("")
            }
            itemBinding.main.setOnClickListener {
                adapterItemClickListener.onPositionClick(searchResponse)
            }
            binder.searchListBind(searchResponse)
            itemBinding.executePendingBindings()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let {
            (holder as DashboardSearchViewHolder).bind(it,position)
        }
    }
}