package com.app.demo

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import com.app.demo.local.DemoDao
import com.app.demo.local.DemoDb
import com.app.demo.network.HttpClientBuilderFactory
import com.app.demo.network.Service
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Provides
    @Singleton
    fun providesService(
        httpClient: HttpClientBuilderFactory
    ): Service = Service.createService(httpClient)

    @Provides
    @Singleton
    fun provideHttpBuilderFactory(@ApplicationContext context: Context): HttpClientBuilderFactory =
        HttpClientBuilderFactory(context)

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    @Provides
    fun provideScannerDb(@ApplicationContext context: Context): DemoDao {
        return DemoDb.getInstance(context).demoDao
    }

    @Provides
    @Singleton
    fun newsRepo(newsService: Service,newsDao: DemoDao) = HomeRepository(newsService,newsDao)
}