package com.app.demo

import androidx.lifecycle.*
import androidx.paging.*
import com.app.demo.network.Item
import com.app.demo.network.Response
import com.app.util.liadatautil.toSingleEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val homeRepository: HomeRepository) : ViewModel(){

    private val _data = MutableLiveData<PagingData<Item>?>()
    val data : LiveData<PagingData<Item>?> = _data

    private val _apiError = MutableLiveData<String>()
    val apiError : LiveData<String> = _apiError.toSingleEvent()

    private val _loading = MutableLiveData<Boolean>()
    val loading : LiveData<Boolean> = _loading.toSingleEvent()

    private val _selectedData = MutableLiveData<Item>()
    val selectedData : LiveData<Item> = _selectedData

    val imageData = MediatorLiveData<ImageData>().apply {
        addSource(_selectedData){
            value = ImageData(url = it.owner?.avatarUrl,null)
        }

    }

    private var listJob: Job? = null
    var viewListJob: Job? = null

    init {
        getUserList()

    }
    fun getUserList() {
        listJob?.cancel()
        listJob =
            viewModelScope.launch {
                _loading.postValue(true)
                homeRepository.getDataList(
                ).cachedIn(viewModelScope).collectLatest {
                    _data.value = it
                    _loading.postValue(false)
                }
            }
    }


    fun updateComment(value: String, id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            homeRepository.updateCommentInDb(value,id)
        }
    }

        fun stopListJob() {
            listJob?.cancel()
            viewListJob?.cancel()
        }

    fun updatePosition(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
           val data =  homeRepository.getSelectedData(id)
            _selectedData.postValue(data)
        }

    }

}