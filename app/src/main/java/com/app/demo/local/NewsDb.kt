package com.app.demo.local

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.app.demo.network.Item


@Database(
    entities = [Item::class],
    version = 1,
    exportSchema = false
)

@TypeConverters(DBTypeConverter::class)

abstract class DemoDb : RoomDatabase(){
    abstract val demoDao : DemoDao

    companion object{
        @Volatile
        private var INSTANCE: DemoDb? = null

        @RequiresApi(Build.VERSION_CODES.KITKAT)
        fun getInstance(context: Context): DemoDb {

            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        DemoDb::class.java,
                        "DEMODB"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }

                return instance
            }
        }
    }
}