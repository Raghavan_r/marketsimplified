package com.app.demo.local

import androidx.room.TypeConverter
import com.app.demo.network.Item
import com.app.demo.network.Owner
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class DBTypeConverter {

    var gson = Gson()

    @TypeConverter
    fun stringToList(data: String?): List<String>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType = object : TypeToken<List<String>?>() {}.type
        return gson.fromJson<List<String>>(data, listType)
    }

    @TypeConverter
    fun listToString(data: List<String>?): String? {
        return gson.toJson(data)
    }

    @TypeConverter
    fun stringToObj(data: String?): Owner? {
        val listType = object : TypeToken<Owner?>() {}.type
        return gson.fromJson<Owner>(data, listType)
    }

    @TypeConverter
    fun objToString(data: Owner): String? {
        return gson.toJson(data)
    }
}