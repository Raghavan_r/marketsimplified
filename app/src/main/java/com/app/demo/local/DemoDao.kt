package com.app.demo.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.app.demo.network.Item
import kotlinx.coroutines.flow.Flow

@Dao
interface DemoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(addItems: List<Item>)

    @Query("SELECT * From Item")
    fun  getAll() : Flow<List<Item>>

    @Query("SELECT * FROM Item WHERE id =:id ")
    suspend fun getDataById(id: Int): Item

    @Query("UPDATE item SET comments = :data WHERE id = :userId ")
    suspend fun updateDataById(data: List<String?>?,userId:Int)
}