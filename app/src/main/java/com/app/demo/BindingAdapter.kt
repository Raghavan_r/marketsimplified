package com.app.demo

import android.text.TextUtils
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
data class ImageData(
    val url:String?,
    val drawable: Int?
        )

object BindingAdapter {

    @JvmStatic
    @BindingAdapter("imageData")
    fun setDrawableImage(view: AppCompatImageView, url : ImageData?) {
        if(!TextUtils.isEmpty(url?.url))
        Glide.with(view.context)
                .load(url?.url)
                .placeholder(R.drawable.ic_outline_account_circle_24)
                .into(view)
        else
            Glide.with(view.context)
                    .load(url?.drawable)
                    .placeholder(R.drawable.ic_outline_account_circle_24)
                    .into(view)

    }

}