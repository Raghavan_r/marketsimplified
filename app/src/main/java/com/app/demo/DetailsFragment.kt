package com.app.demo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import com.app.demo.databinding.FragmentDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

private lateinit var binding: FragmentDetailsBinding

@AndroidEntryPoint
class DetailsFragment : BaseFragment() {

    private val homeViewModel: HomeViewModel by activityViewModels()

    private val adapter by lazy {
        DetailsAdapter( this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.fragment_details,
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.viewModel = homeViewModel

        val position = arguments?.getInt("ID_DATA",0)
        position?.let {
            homeViewModel.updatePosition(it)
        }
        binding.rvComments.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeViewModel.selectedData.observe(viewLifecycleOwner,{
            adapter.setDataList(it.comments ?: emptyList())
        })
    }

}