package com.app.demo.network

import android.content.Context
import com.app.demo.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit


class HttpClientBuilderFactory(private val context : Context) {

    private val okHttpClient by lazy { OkHttpClient() }

    fun create(): OkHttpClient.Builder = okHttpClient.newBuilder().apply {
        connectTimeout(BuildConfig.TIME_OUT, TimeUnit.SECONDS)
        readTimeout(BuildConfig.TIME_OUT, TimeUnit.SECONDS)
        if(BuildConfig.DEBUG)
        addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
    }
}