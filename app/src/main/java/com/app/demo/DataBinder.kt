package com.app.demo

import android.text.TextUtils
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.app.demo.network.Item
import com.app.demo.network.Response

class DataBinder {

    private var searchData = MutableLiveData<Item>()

    private var detailsData = MutableLiveData<String>()
    val _detailsData : LiveData<String> = detailsData

    val name: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(searchData) {
            value = it.name
        }
    }

    val description: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(searchData) {
            value = it.description
        }
    }


    val coverImage: LiveData<ImageData> = MediatorLiveData<ImageData>().apply {
        addSource(searchData) {
            value = if (!TextUtils.isEmpty(it.owner?.avatarUrl)){
                ImageData(url = it.owner?.avatarUrl, drawable = null)
            }else{
                ImageData(url = null, drawable = R.drawable.ic_baseline_details_24)

            }
        }
    }


    @MainThread
    fun searchListBind(searchResponse: Item) {
        searchData.value = searchResponse
    }

    @MainThread
    fun detailsBind(searchResponse: String) {
        detailsData.value = searchResponse
    }
}